import React from "react";
import ReactDOM from "react-dom";

class Index extends React.Component {
  a = 23;
  b = 27;
  add = (a, b) => {
    console.log(this.a + this.b);
  }
  minus = (a, b) => {
    console.log(this.a - this.b);
  }
  bol = (a, b) => {
    console.log(this.a / this.b);
  }
  kop = (a, b) => {
    console.log(this.a * this.b);
  }
  render() {
    return (
      <div className="container">
        <button className="btn btn-primary m-1" onClick={this.add}>Qoshish</button>
        <button className="btn btn-primary m-1" onClick={this.minus}>Ayirish</button>
        <button className="btn btn-primary m-1" onClick={this.bol}>Bolish</button>
        <button className="btn btn-primary m-1" onClick={this.kop}>Kopaytirish</button>
      </div>
    )
  };
}

ReactDOM.render(
  <Index />,
  document.querySelector('#root')
)
