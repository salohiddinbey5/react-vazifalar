import React from "react";
import ReactDOM  from "react";

class Index extends React.Component{
  render(){
    <div>
      <button className="btn btn-succes">Click Me</button>
    </div>
  }
}

ReactDOM.render(
  <Index />,
  document.querySelector('#root')
)